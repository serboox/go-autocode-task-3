package main

import (
	"testing"
)

type testCaseItem struct {
	data   map[int]string
	result []string
}

var testCases = []testCaseItem{
	{
		map[int]string{2: "a", 0: "b", 1: "c"},
		[]string{"b", "c", "a"},
	},
	{
		map[int]string{10: "aa", 0: "bb", 500: "cc"},
		[]string{"bb", "aa", "cc"},
	},
	{
		map[int]string{21: "aaa", 14: "bbb", 7: "ccc"},
		[]string{"ccc", "bbb", "aaa"},
	},
}

func TestPrintSorted(t *testing.T) {
	for _, testCaseItem := range testCases {
		resultSlice := printSorted(testCaseItem.data)
		for i, value := range resultSlice {
			if testCaseItem.result[i] != value {
				t.Fatal(
					"For", testCaseItem.data,
					"expected", testCaseItem.result,
					"got", resultSlice,
				)
			}
		}
	}
}
